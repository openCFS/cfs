Linear Form  ([back to main page](/source/README.md))
============

Linear forms define the right hand side terms of a PDE in its weak formulation. A typical example looks like

![](/share/doc/developer/pages/pics/TypicalLinearForm.png)

Link to [doxygen-documentation](https://opencfs.gitlab.io/cfs/doxygen/classCoupledField_1_1LinearForm.html); for the source see [LinearForm-Source](/source/Forms/LinForms/LinearForm.hh)
